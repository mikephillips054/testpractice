
//Definition of the "TV" class
class TV {

    //"default" getter and setter for the "_TVMake" property
    get TVMake() {
        return this._tvMake
    }

    set TVMake(value) {
        this._tvMake = value
    }

    //"default" getter and setter for the "_tvModel" property
    get TVModel() {
        return this._tvModel
    }

    set TVModel(value) {
        this._tvModel = value
    }
    
    //"default" getter and setter for the "_tvYear" property
    get TVYear() {
        return this._tvYear
    }

    set TVYear(value) {
        this._tvYear = value
    }
    
    //"default" getter and setter for the "_ScreenSize" property
    get ScreenSize() {
        return this._screenSize
    }

    set ScreenSize(value) {
        this._screenSize = value
    }
    
    //"default" getter and setter for the "_Price" property
    get Price() {
        return this._price
    }

    set Price(value) {
        this._price = value
    }


    //Constructor for the "TV" class
    constructor(tvMake, tvModel, tvYear, screenSize, price){
        this._tvMake  = tvMake
        this._tvModel = tvModel
        this._tvYear = tvYear
        this._screenSize = screenSize
        this._price = price
    }
ccPrice(_levy){
    return (1 + (_levy / 100)) * this.Price;
}
}


//Instantiated a "blank" new object "newTV" using the "TV" class as a "template/blueprint"
let newTV = new TV()


//Prompt user for "newTV" property values displayed to browser console as they are entered
newTV.TVMake  = prompt("TV Warehouse Ltd: Enter Product Details \nEnter the TV Make:")
newTV.TVModel = prompt("TV Warehouse Ltd: Enter Product Details \nEnter the TV Model:")
newTV.TVYear = prompt("TV Warehouse Ltd: Enter Product Details \nEnter the TV Year:")
newTV.ScreenSize = prompt("TV Warehouse Ltd: Enter Product Details \nEnter the Screen Size:")
newTV.Price = Number(prompt("TV Warehouse Ltd: Enter Product Details \nEnter the Price:"))

//Outputting to console 
console.log(`
                  TV Warehouse Ltd: Product Details
        ----------------------------------------------------

        TV Make:            ${newTV.TVMake}
        TV Model:           ${newTV.TVModel}
        TV Year:            ${newTV.TVYear}
        TV Size :           ${newTV.ScreenSize} inches
        TV Price:           $${newTV.Price}

        ----------------------------------------------------
        
        
`)

//Propmting  user for credit levy and output
levy = prompt(`TV Warehouse Ltd. \nEnter Credit Levy (%):`)

console.log(`Credit Price: $`, newTV.ccPrice(levy));
